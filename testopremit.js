const readline = require("readline");
const rl = readline.createInterface({
 input: process.stdin,
 output: process.stdout,
});

// 1==================================================

const autoriProgram = (inputValue) => {
 const text = inputValue.split("-");
 let shortVariation = text[0][0];
 for (let i = 1; i < text.length; i++) {
  shortVariation += text[i][0];
 }
 return shortVariation.toUpperCase();
};

const autori1 = "Knuth-Morris-Pratt";
const autori2 = "Patar-Siahaan";
const showAutori = autoriProgram(autori2);
console.log("showAutori\t\t:", showAutori);

// 2==================================================

const backspaceProgram = (inputValue) => {
 const stack = [];
 for (const char of inputValue) {
  if (char === "<") {
   stack.pop();
  } else {
   stack.push(char);
  }
 }
 return stack.join("");
};

const backspace1 = "a<bc<";
const backspace2 = "foss<<rritun";
const backspace3 = "a<a<a<a<a<<";
const checkBackspace = backspaceProgram(backspace2);
console.log("showBackspace\t\t:", checkBackspace);

// 3==================================================

const keyToCryptoProgram = (ciphertext, key) => {
 let plaintext = "";
 let keyIndex = 0;

 for (let i = 0; i < ciphertext.length; i++) {
  let ciphertextCharCode = ciphertext.charCodeAt(i) - 65;
  let keyCharCode = key.charCodeAt(keyIndex) - 65;
  let shift = (ciphertextCharCode - keyCharCode + 26) % 26;

  plaintext += String.fromCharCode(shift + 65);

  if (keyIndex < key.length) {
   key += String.fromCharCode(shift + 65);
   keyIndex++;
  }
 }

 return plaintext;
};

let ciphertext = "SGZVQBUQAFRWSLC";
let secretWord = "ACM";

let encryptText = keyToCryptoProgram(ciphertext, secretWord);

console.log("showKeyToCryptoProgram\t:", encryptText);

// 4==================================================

const quadrantSelectionProgram = (x, y) => {
 if (x > 0) {
  console.log("Quadrant number is\t:", y > 0 ? 1 : 4);
 } else {
  console.log("Quadrant number is\t:", y > 0 ? 2 : 3);
 }
};

rl.question("Enter the x coordinate\t: ", (x) => {
 rl.question("Enter the y coordinate\t: ", (y) => {
  quadrantSelectionProgram(parseInt(x), parseInt(y));
  rl.close();
 });
});

// 5==================================================

rl.on("close", () => {
 function countEachLetter(input) {
  let count = {};
  let hasil = "";

  for (let i = 0; i < input.length; i++) {
   let char = input[i];
   if (char !== " ") {
    count[char] = (count[char] || 0) + 1;
   }
  }

  for (let key in count) {
   hasil += key + count[key];
  }

  return hasil;
 }

 let input = "E   HHHeellloWooorrrrlld!!";
 let output = countEachLetter(input);
 console.log("Your text\t\t:", input);
 console.log("Output\t\t\t:", output);
});
